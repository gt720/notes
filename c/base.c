
/*
compile
gcc -Wall -g -lm qe.c -o qe 
- -lm - add library
- -Wall -g - debugging flags
*/
  
// https://codecast.france-ioi.org/v6/

//  ## Pointers

int a, *b, c, d //  we declare that b is indeed a pointer to an integer
b = &a; // unary operator `&' is used to produce the address of object, if it has one
c = *b; //  means to use the value in b as an address
d = b // b contain address on 0x, and it convert to decimal integer


#include <stdio.h>
int main(void) {
    
    int array[] = {6, 2, -4, 8, 5, 1};
    int *ptr, *ptr2;
    
    printf("Array contains %d, %d, ... , %d\n", array[0], array[1], array[5]);
    printf("These are stored at %p, %p, ..., %p\n", &array[0], &array[1], &array[5]);
    // array equals &array[0]
    ptr = array;
    ptr2 = &array[0];
    
    *ptr = 10;
    *(ptr + 1) = 5; 
    *(ptr + 2) = -1;
    
    *array = 3;
    *(array + 1) = 10;
    *(array + 2) = 99;
    
    ptr++;
    *ptr = 7;
    
    ptr += 3;
    *ptr = 8;
    return 0;
}


// Memory alloc

void *malloc(int size);
void free(void *p);

double *k;
k = malloc(30*sizeof(double));

free(k);



