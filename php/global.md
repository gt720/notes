

``` bash
php -r "echo ini_get('memory_limit').PHP_EOL;"

# php.ini
memory_limit = -1

php -d memory_limit=-1 composer ....
COMPOSER_MEMORY_LIMIT=-1 composer ...

php -r "copy('https://getcomposer.org/composer-stable.phar', 'composer.phar');"
```



xdebug 2 config
```ini
; ENVIRONMENT: dev
zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so
xdebug.remote_autostart=on
xdebug.remote_enable=on
xdebug.remote_handler="dbgp"
; DEV ONLY
; any client may run remote debug
xdebug.remote_connect_back=0
xdebug.remote_port=9001
xdebug.remote_mode=req
xdebug.client_host=172.17.0.1
```

xdebug 3 config
```ini
; ENVIRONMENT: dev
zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so
xdebug.start_with_request=yes
xdebug.discover_client_host=no
xdebug.client_port=9001
xdebug.mode=debug
xdebug.client_host=172.17.0.1
xdebug.log_level=0
```
