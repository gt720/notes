
```php
// print sql
vsprintf(str_replace(['?'], ['\'%s\''], $query->toSql()), $query->getBindings());
```


```php
private function joinTree($email, int $deep = 3)
{
    #
    $select = [
        "0.id as id0",
        "0.email as email0",
    ];

    $q = DB::table('managers as 0')->select($select)->where('0.email', $email);

    for ($i = 1; $deep >= $i; $i++) {

        $i_parent = $i - 1;
        $i_select = array_map(function ($q) use ($i) {
            return str_replace('0', $i, $q);
        }, $select);

        $q->addSelect($i_select)->leftJoin("managers as $i", "$i_parent.id", '=', "$i.head_manager_id");
    }

    $result = $q->first();

    if (empty($result)) {
        return collect();
    }

    #
    $data = [];
    for ($i = 0; $deep >= $i; $i++) {

        $email = data_get($result, "email{$i}");
        $id = data_get($result, "id{$i}");

        if (!empty($email) || !empty($id)) {
            $data[] = compact('id', 'email');
        }
    }

    return collect($data);
}
```
