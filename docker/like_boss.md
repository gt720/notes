```sh
docker container run --publish 80:80 nginx
docker container run --publish 80:80 --detach nginx
docker container run --publish 80:80 --detach --name webserver nginx
docker container run --publish 8080:80 --detach --name webserver nginx:1.11 nginx -T
docker run --rm -it centos:7 bash
docker container ls -a

docker container logs webserver #logs
docker container top webserver #process list in container

docker container rm  ff8 945 b401
docker container rm -f ff8 945 b401
```


```sh
# inspect
docker container inspect webserver # get details json info
docker container inspect webserver --format '{{ .NetworkSettings.IPAddress }}' # go tamplates
docker container stats  # perfomace ps info

# bash or sh
docker container run -it --name proxy nginx bash # run bash inside container
docker container exec -it mysql bash # existing container
docker container start -ai ubuntu #

#network
# -p (--publish) HOST:CONTAINER format
docker network ls # list
docker network inspect bridge # get details json info
docker network create test_network # create VN
docker network connect test_network webserver #connect container to VN
docker network disconnect test_network webserver # dicsonect

#images
docker image ls
docker image history nginx:latest # show layers of changes made in image
docker image inspect nginx:latest # json meta data about image
#tag
docker image tag nginx:latest tima\nginx:latest # add new tag to ImageID
docker image push tima\nginx:latest # push image to Docker Hub
docker image tag tima\nginx:latest tima\nginx:test
docker image push tima\nginx:test
```

### Volumes
```sh
docker image inspect; docker container inspect # can show info about volumes Mounts,Volumes section
docker volume ls # list of volumes
docker volume create test_volume
docker container run -v test_volume:/usr/share/nginx/html/ nginx # conect volume, if inspect you can find this folder on host machine

#mounting
docker container run  -v /home/.../index.html:/usr/share/nginx/html/index.html nginx # mounting
docker container run  -v $(pwd):/usr/share/nginx/html nginx
```

### Tasks
```sh
#task2
docker network create test_network
docker run -d --network test_network --network-alias search elasticsearch:2
docker run --rm --network test_network alpine nslookup search
docker run --rm --network test_network centos:7 curl -s search:9200
```



