[Udemy](https://www.udemy.com/course/datastructurescncpp/)
[Coursehunter](https://coursehunter.net/course/ctruktury-dannyh-i-algoritmy-s-ispolzovaniem-c-i-c)


# Recursion

## Tail recursion
```c
void func(int n){
    if(n > 0){
        printf("%d ", n); // run in call phase
        func(n - 1); 
    }
}
void main(){
    int x = 3;
    func(x);
}
```

## Head recursion
```c
void func(int n){
    if(n > 0){
        func(n - 1);
        printf("%d ", n); // run in return phase
    }
}
void main(){
    int x = 3;
    func(x);
}
```

## Tree recursion
```c
void func(int n) {
    if (n>0) {
        printf("%d ", n);
        func(n-1);
        func(n-1);
    }
}
void main() {
    func(3);
}
```

## Indirect recursion
```c
#include <stdio.h>

void funB(int n);
void funA(int n)
{
    if (n>0)
    {
        printf("%d ", n);
        funB(n-1);
    }
}
void funB(int n)
{
    if (n>1)
    {
        printf("%d ", n);
        funA(n/2);
    }
}
int main()
{
    funA(20);
    return 0;
}
// 20 19 9 8 4 3 1 
```

## Nested recursion
```c
#include <stdio.h>
int fun(int n)
{
    if (n>100) {
        return n-10;
    }
    // int x = fun(n+11);
    // return fun(x);
    return fun(fun(n+11));
}
int main()
{
    printf("%d\n", fun(95));
    return 0;
}
// 91
```

## Sum of Natural Number
```c
// 1+2+3+4+5+6+n = 
sum1(int n){
    return n*(n+1)/2;
}
sum2(int n){
    if(n==0)
        return 0;
    return sum2(n-1)+n;
}
sum3(int n){
    int s = 0, i;
    for(i=1; i<=n;i++)
        s=s+i;
    return s;
}
```

## Factorial
```c
// n! = 1*2*3*4*5*6*n 
fac1(int n){
    if(n==0)
        return 1;
    return sum2(n-1)*n;
}
fac2(int n){
    int s = 1, i;
    for(i=1; i<=n;i++)
        s=s*i;
    return s;
}
```

## Power(Степень)
```c
// n! = 1*2*3*4*5*6*n 
pow(int n, int p){
    if(p==0)
        return 1;
    return  pow(n, p-1)*n;
}
```


## Fibonacci
```c
// 0 1 1 2 3 5 8 13
int fib(int n) {
    if (n > 1)
        return fib(n - 2) + fib(n - 1);
    return n;
}
int fib2(int n) {

    if (n <= 1) {
        return n;
    }

    int t1 = 0, t2 = 1, s, i;
    for (i = 2; i <= n; i++) {
        s = t1+t2;
        t1 = t2;
        t2 = s;
    }

    return s;
}
```
