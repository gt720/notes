
# Rclone - sync app for linux

https://rclone.org/yandex/

```
rclone config
rclone lsd ydisk
rclone sync /home/user1/syncLinux/ ydisk:syncLinux

crontab -e
0 * * * * /usr/bin/rclone sync /home/user1/syncLinux/ ydisk:syncLinux
```
